# kong-plugin-upstream-redirect

- **System owner** [Rolf](mailto:rolf.blindheim@inonit.no)

**Contribution**

This work is based on [kong-dynamic-upstream](https://github.com/nvmlabs/kong-dynamic-upstream).

## Installing

To install the plugin, enter the following command in the `kong-plugin`
directory.

    $ make install

## Production setup

In order to let nginx pick up the new `proxy_pass` endpoint, you'll have to provide a custom nginx config.
The required entries are listed below.

    <snip>
    server {
        server_name kong;

        <snip>

        location / {
            # We need a resolver outside of kong in order to lookup
            # external hosts.
            resolver 8.8.8.8;

            # This context variable is set by the plugin in order
            # to infor nginx that we should do a custom proxy pass.
            set $upstream_redirect_host_header nil;

            access_by_lua_block {
                kong.access()
            }

            <snip>

            # Put this just before the original proxy_pass statement.
            # It will check if the $upstream_redirect_host_header context
            # variable has been set, and proxy the request accordingly.
            if ($upstream_redirect_host_header) {
                proxy_pass $upstream_scheme://$upstream_host$upstream_uri;
            }

            # Original proxy pass statement
            proxy_pass $upstream_scheme://kong_upstream$upstream_uri;

            <snip>
        }
    }
    <snip>

Next, enable the plugin and enable it on api endpoints as outlined below.

## Development setup

1. **Install vagrant**

    Download and install [Vagrant](https://www.vagrantup.com/downloads.html) from their site or use your operating system package manager.

2. **Clone the Kong repository in this directory**

        $ git clone https://github.com/Mashape/kong

3. **Start the vagrant box**

        $ vagrant up

    This will mount the `kong` and `kong-plugin` directories in the vm.

4. **Log into the vm**

    Once the vm is done provisioning ssh into the vm and make sure both `kong` and `kong-plugin` are mounted on `/kong` and `/kong-plugin`.

        $ vagrant ssh
        vagrant@vagrant-ubuntu-trusty-64:~$ mount
        <snip>
        kong on /kong type vboxsf (uid=1000,gid=1000,rw)
        vagrant on /vagrant type vboxsf (uid=1000,gid=1000,rw)
        kong-plugin on /kong-plugin type vboxsf (uid=1000,gid=1000,rw)

5. **Ready the vm for development**

    Still logged into the vm we need to make it ready to run Kong with the plugin enabled.

    - Install Kong dependencies

            $ cd /kong
            $ make dev

    - Install plugin dependencies

            $ cd /kong-plugin
            $ make dev

    - Tell Kong to load the plugin

            $ export KONG_CUSTOM_PLUGINS=upstream-redirect

    - Start Kong with the custom nginx template

            $ cd /kong
            $ bin/kong start --nginx-conf custom_nginx.template

6. **Register an API endpoint in Kong**

    We need an endpoint to test on.

        $ curl -i -X POST \
            --url http://localhost:8001/apis/ \
            --data 'name=example-api' \
            --data 'hosts=example.com' \
            --data 'upstream_url=http://example.com'

    This will create an endpoint in Kong which will proxy requests to `http://example.com`.
    This is not particularly useful, so we'll use the `upstream-redirect` plugin to change it on the fly.

    Next, enable the `upstream-redirect` plugin to the endpoint.

        $ curl -i -X POST \
            --url http://localhost:8001/apis/example-api/plugins/ \
            --data 'name=upstream-redirect' \
            --data 'conf.upstream_host_header=x-upstream-redirect'

    This will enable the plugin on the API and configure it to look for a `X-Upstream-Redirect` header on incoming requests.
    This header should hold the full path to the new custom endpoint, ie. `https://mockbin.com/request`.

7. **Make sure it works**

    First try to make a request to the endpoint without the `X-Upstream-Redirect` header.

        $ curl -i -X GET \
            --url http://localhost:8000/ \
            --header 'Host: example.com'

    You should receive the html output from `http://example.com`.

    Next, try to pass the `X-Upstream-Redirect` header.

        $ curl -i -X GET \
            --url http://localhost:8000/ \
            --header 'Host: example.com' \
            --header 'X-Upstream-Redirect: http://mockbin.com/request'

        HTTP/1.1 200 OK
        Date: Fri, 14 Jul 2017 12:06:14 GMT
        Content-Type: application/json; charset=utf-8
        Content-Length: 993
        Connection: keep-alive
        Set-Cookie: __cfduid=df135dd525a485d89b3f8954336596c291500033974; expires=Sat, 14-Jul-18 12:06:14 GMT; path=/; domain=.mockbin.com; HttpOnly
        Access-Control-Allow-Origin: *
        Access-Control-Allow-Methods: GET
        Access-Control-Allow-Headers: host,connection,accept-encoding,x-forwarded-for,cf-ray,x-forwarded-proto,cf-visitor,user-agent,accept,x-upstream-redirect,cf-connecting-ip,x-request-id,x-forwarded-port,via,connect-time,x-request-start,total-route-time
        Access-Control-Allow-Credentials: true
        X-Powered-By: mockbin
        Vary: Accept, Accept-Encoding
        Etag: W/"3e1-Qq7GQX0eXms6HlaDE+L3bQ"
        Via: kong/0.10.3
        Server: cloudflare-nginx
        CF-RAY: 37e45ad4a6d04273-OSL
        X-Kong-Upstream-Latency: 319
        X-Kong-Proxy-Latency: 603

        {
          "startedDateTime": "2017-07-14T12:06:14.708Z",
          "clientIPAddress": "127.0.0.1",
          "method": "GET",
          "url": "http://mockbin.com/request",
          "httpVersion": "HTTP/1.1",
          "cookies": {},
          "headers": {
            "host": "mockbin.com",
            "connection": "close",
            "accept-encoding": "gzip",
            "x-forwarded-for": "127.0.0.1,81.166.160.174, 162.158.222.83",
            "cf-ray": "37e45ad4a6d04273-OSL",
            "x-forwarded-proto": "http",
            "cf-visitor": "{\"scheme\":\"http\"}",
            "user-agent": "curl/7.35.0",
            "accept": "*/*",
            "x-upstream-redirect": "http://mockbin.com/request",
            "cf-connecting-ip": "81.166.160.174",
            "x-request-id": "c0a8ae9d-c4b6-44da-bd6a-446e14fa987d",
            "x-forwarded-port": "80",
            "via": "1.1 vegur",
            "connect-time": "0",
            "x-request-start": "1500033974705",
            "total-route-time": "0"
          },
          "queryString": {},
          "postData": {
            "mimeType": "application/octet-stream",
            "text": "",
            "params": []
          },
          "headersSize": 528,
          "bodySize": 0
        }

    You should now receive the response from `http://mockbin.com/request` instead.
